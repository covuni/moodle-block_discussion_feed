<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Settings for Block Discussion Feed.
 *
 * @package   block_discussion_feed
 * @copyright 2018 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_configtext('block_discussion_feed' . '/discussionagedays',
        get_string('discussionagedays', 'block_discussion_feed'),
        get_string('discussionagedaysdesc', 'block_discussion_feed'), DISCUSSION_FEED_DISCUSSION_AGE_DAYS_DEFAULT, PARAM_INT));

    $settings->add(new admin_setting_configtext('block_discussion_feed' . '/maxpostlengthtodisplay',
        get_string('maxpostlengthtodisplay', 'block_discussion_feed'),
        get_string('maxpostlengthtodisplaydesc', 'block_discussion_feed'), 0, PARAM_INT));

    $settings->add(new admin_setting_configcheckbox('block_discussion_feed' . '/usecaching',
        get_string('usecaching', 'block_discussion_feed'),
        get_string('usecachingdesc', 'block_discussion_feed'), 1));

    $settings->add(new admin_setting_configtext('block_discussion_feed' . '/cachingttl',
        get_string('cachingttl', 'block_discussion_feed'),
        get_string('cachingttldesc', 'block_discussion_feed'), DISCUSSION_FEED_CACHE_TTL_DEFAULT, PARAM_INT));

    // Allow on sections.
    $settings->add(new admin_setting_configcheckbox('block_discussion_feed' . '/displayinsections',
        get_string('displayinsections', 'block_discussion_feed'),
        get_string('displayinsectionsdesc', 'block_discussion_feed'), 1));

    $settings->add(new admin_setting_configcheckbox('block_discussion_feed' . '/showexpandtoggle',
        get_string('showexpandtoggle', 'block_discussion_feed'),
        get_string('showexpandtoggledesc', 'block_discussion_feed'), 1));

    $settings->add(new admin_setting_configtext('block_discussion_feed' . '/excludeforumids',
            get_string('excludeforumids', 'block_discussion_feed'), get_string('excludeforumidsdesc',
                    'block_discussion_feed'), '', PARAM_TEXT));

}