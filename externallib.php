<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External API.
 *
 * This is used by the block_discussion_feed plugin itself in javascript to perform various functions such as
 * displaying discussion posts.
 *
 * @package    block_discussion_feed
 * @copyright  2018 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot . "/blocks/discussion_feed/lib.php");
require_once($CFG->dirroot . "/blocks/course_discuss/lib.php");
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->dirroot . '/mod/forum/externallib.php');

use context_system;
use external_api;
use external_function_parameters;
use external_value;
use external_single_structure;
use external_multiple_structure;

/**
 * External API class.
 *
 * @package    block_discussion_feed
 * @copyright  2018 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
class block_discussion_feed_external extends external_api {

    /**
     * Returns description of create_discussion() parameters.
     *
     * @return \external_function_parameters
     */
    public static function create_discussion_parameters() {
        return new external_function_parameters(
            array(
                'forumid' => new external_value(PARAM_INT, 'Forum ID', VALUE_DEFAULT, ''),
                'message' => new external_value(PARAM_TEXT, 'Message', VALUE_DEFAULT, ''),
                'subject' => new external_value(PARAM_TEXT, 'Subject', VALUE_DEFAULT, ''),
                'groupid' => new external_value(PARAM_INT, 'Group ID', VALUE_DEFAULT, '-1')
            )
            );
    }

    /**
     * Returns description of create_discussion_returns() result value.
     *
     * @return \external_description
     */
    public static function create_discussion_returns() {

        return new external_single_structure(
            array(
            'discussionid' => new external_value(PARAM_INT, 'New Discussion ID'),
            'warnings' => new external_warnings()
            )
            );

    }

    /**
     * Create discussion.
     *
     * @param string $courseid
     * @param int    $forumid
     * @param int    $contextid
     * @param string $message
     * @param string $subject
     * @param int    $groupid
     * @param string $organisation
     * @return mixed
     */
    public static function create_discussion($forumid = '', $message, $subject, $groupid, $organisation = '') {

        global $PAGE, $DB, $OUTPUT;

        require_once(dirname(dirname(__DIR__)).'/config.php');

        // Clean parameters.
        $params = self::validate_parameters(self::create_discussion_parameters(), array(
            'forumid' => $forumid, 'subject' => $subject, 'message' => $message, 'groupid' => $groupid));

        $createddiscussion = mod_forum_external::add_discussion($forumid, $subject, $message, $groupid);
        $createddiscussion = external_api::clean_returnvalue(mod_forum_external::add_discussion_returns(), $createddiscussion);

        return $createddiscussion;

    }

    /**
     * Returns description of display_all_discussions() parameters.
     *
     * @return \external_function_parameters
     */
    public static function display_all_discussions_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID', VALUE_DEFAULT, ''),
                'forumid' => new external_value(PARAM_INT, 'Forum ID', VALUE_DEFAULT, ''),
                'shownewdiscussionform' => new external_value(PARAM_BOOL, 'Show new discussion form', VALUE_DEFAULT, '')
            )
            );
    }

    /**
     * Display all discussions for a course.
     *
     * @param string $courseid
     * @param string $forumid
     * @param string $shownewdiscussionform
     * @param string $component
     * @return mixed
     */
    public static function display_all_discussions($courseid, $forumid, $shownewdiscussionform = true,
                           $component = 'block_discussion_feed') {

        global $PAGE, $DB, $CFG;

        require_once(dirname(dirname(__DIR__)).'/config.php');

        $params = self::validate_parameters(self::display_all_discussions_parameters(), array(
            'courseid' => $courseid, 'forumid' => $forumid,
            'shownewdiscussionform' => $shownewdiscussionform
        ));

        $courses = array();

        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $courses[] = $course;
        $cm = get_coursemodule_from_instance('forum', $forumid, $courseid, false, MUST_EXIST);

        require_login($course, true, $cm);

        $content = '';

        if ($shownewdiscussionform) {
            // Display form for a new discussion.
            $discussionid = 0;
            $parentpostid = 0;
            $content .= '<div id="block_discussion_feed_new_discussion_container">';
            $forum = $DB->get_record('forum', array('id' => $forumid), '*', MUST_EXIST);
            $content .= block_discussion_feed_display_new_discussion_form($cm, $forum, $course,
                        'discussion-feed-create-post');
            $content .= '</div><div id="comment_0"></div>';
        }

        $config = get_config("block_discussion_feed");
        // Number of days in the past to get discussions for.
        if (!empty ($config->discussionagedays)) {
            $discussionagedays = $config->discussionagedays;
        } else {
            $discussionagedays = DISCUSSION_FEED_DISCUSSION_AGE_DAYS_DEFAULT;
        }

        // Call library function that gets all the discussions in HTML format.
        $content .= '<div class="block_discussion_feed_container">';
        $content .= block_discussion_feed_get_latest_discussions($courses, $discussionagedays);
        $content .= '</div>';

        return array(
            'eventaction'      => 'discussionreturned',
            'html'             => $content,
            'warning'          => '',
            'errormessage'     => ''
        );

    }

    /**
     * Returns description of display_discussion_returns() result value.
     *
     * @return \external_description
     */
    public static function display_all_discussions_returns() {

        return new external_single_structure(
            array(
                'eventaction'                     => new external_value(PARAM_TEXT, 'Event action status'),
                'warning'                         => new external_value(PARAM_RAW, 'Any warning messages'),
                'html'                            => new external_value(PARAM_RAW, 'Returned HTML format new content'),
                'errormessage'                    => new external_value(PARAM_RAW, 'Any error messages')
            )
            );

    }


    /**
     * Returns description of display_discussion() parameters.
     *
     * @return \external_function_parameters
     */
    public static function display_discussion_posts_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID', VALUE_DEFAULT, ''),
                'forumid' => new external_value(PARAM_INT, 'Forum ID', VALUE_DEFAULT, ''),
                'discussionid' => new external_value(PARAM_INT, 'Discussion ID', VALUE_DEFAULT, ''),

            )
            );
    }

    /**
     * Display a discussion.
     *
     * @param string $courseid
     * @param string $forumid
     * @param int    $discussionid
     * @param string $component
     * @return mixed
     */
    public static function display_discussion_posts($courseid, $forumid, $discussionid,
                                              $component = 'block_discussion_feed') {

        global $PAGE, $DB;

        require_once(dirname(dirname(__DIR__)).'/config.php');

        $params = self::validate_parameters(self::display_discussion_posts_parameters(), array(
            'courseid' => $courseid, 'forumid' => $forumid,
            'discussionid' => $discussionid
        ));

        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $forum = $DB->get_record('forum', array('id' => $forumid), '*', MUST_EXIST);
        $discussion = $DB->get_record('forum_discussions', array('id' => $discussionid), '*', MUST_EXIST);
        $cm = get_coursemodule_from_instance('forum', $forumid, $courseid, false, MUST_EXIST);

        require_login($course, true, $cm);

        // Call library function that gets the discussion in HTML format.
        $content = block_discussion_feed_display_discussion($cm, $course, $discussion, $forum);

        return array(
            'eventaction'      => 'discussionreturned',
            'html'             => $content,
            'warning'          => '',
            'errormessage'     => ''
        );

    }

    /**
     * Returns description of display_discussion_returns() result value.
     *
     * @return \external_description
     */
    public static function display_discussion_posts_returns() {

        return new external_single_structure(
            array(
                'eventaction'                     => new external_value(PARAM_TEXT, 'Event action status'),
                'warning'                         => new external_value(PARAM_RAW, 'Any warning messages'),
                'html'                            => new external_value(PARAM_RAW, 'Returned HTML format new content'),
                'errormessage'                    => new external_value(PARAM_RAW, 'Any error messages')
            )
            );

    }

    /**
     * Returns description of display_reply_form() parameters.
     *
     * @return \external_function_parameters
     */
    public static function display_reply_form_parameters() {

        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID', VALUE_DEFAULT, ''),
                'forumid' => new external_value(PARAM_INT, 'Forum ID', VALUE_DEFAULT, ''),
                'discussionid' => new external_value(PARAM_INT, 'Discussion ID', VALUE_DEFAULT, ''),
                'parentpostid' => new external_value(PARAM_INT, 'Parent post ID', VALUE_DEFAULT, '')
            )
            );
    }

    /**
     * Display a reply comment form for a specific post.
     *
     * @param int     $courseid
     * @param int     $forumid
     * @param int     $discussionid
     * @param int     $parentpostid
     * @param string  $component
     * @return mixed
     */
    public static function display_reply_form($courseid, $forumid, $discussionid, $parentpostid,
                           $component = 'block_discussion_feed') {

        global $PAGE, $DB;

        require_once(dirname(dirname(__DIR__)).'/config.php');

        // Clean parameters.
        $params = self::validate_parameters(self::display_reply_form_parameters(), array(
            'courseid' => $courseid,
            'forumid' => $forumid,
            'discussionid' => $discussionid,
            'parentpostid' => $parentpostid
        ));

        $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $forum = $DB->get_record('forum', array('id' => $forumid), '*', MUST_EXIST);
        $discussion = $DB->get_record('forum_discussions', array('id' => $discussionid), '*', MUST_EXIST);
        $cm = get_coursemodule_from_instance('forum', $forumid, $courseid, false, MUST_EXIST);

        require_login($course, true, $cm);

        $content = block_course_discuss_display_new_comment_form($cm, $forumid, 0, "",
                   'discussion-feed-create-reply', $discussionid, $parentpostid);

        return array(
            'eventaction'      => 'formreturned',
            'html'             => $content,
            'warning'          => '',
            'errormessage'     => ''
        );

    }

    /**
     * Returns description of display_reply_form_returns() result value.
     *
     * @return \external_description
     */
    public static function display_reply_form_returns() {
        return new external_single_structure(
            array(
                'eventaction'                     => new external_value(PARAM_TEXT, 'Event action status'),
                'html'                            => new external_value(PARAM_RAW, 'Returned html'),
                'warning'                         => new external_value(PARAM_RAW, 'Any warning messages'),
                'errormessage'                    => new external_value(PARAM_RAW, 'Any error messages')
            )
            );
    }

}
