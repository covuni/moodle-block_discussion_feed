// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles all events for replying to comments and creating discussions.  Uses the web services
 * provided by block_discussion_feed.
 *
 * @module     block_discussion_feed/discussion_feed
 * @class      discussion_feed
 * @package    block_discussion_feed
 * @copyright  2018 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      3.4
 */

define(['jquery', 'core/ajax'], function($, ajax) {

    return {

        init: function(langstrings) {

            // For max number of characters in a textarea input.
            $(document).ready(function() {
                var textMax = 1200;
                $('#block_discussion_feed_form_message_feedback').html(textMax + langstrings.charactersremainingtext);

                $('#block_discussion_feed_form_message').keyup(function() {
                    var textLength = $('#block_discussion_feed_form_message').val().length;
                    var textRemaining = textMax - textLength;
                    $('#block_discussion_feed_form_message_feedback').html(textRemaining + langstrings.charactersremainingtext);
                });

                if (($('.block_discussion_feed_posts_container_content')).length > 0) {
                    // We use an arbitrary length of text here to check for hiding the expand button.
                    if ($('.block_discussion_feed_posts_container_content').text().length < 600) {
                        $(".block_discussion_feed_expand_container").hide();
                    }
                }

            });

            // Handle click on various buttons within the main container.
            $(".block_discussion_feed_container").on("click", function(event) {

                // Reply button.
                if ((event.target.getAttribute("class") == "block_discussion_feed_action_button")) {
                    var postid = $(event.target).val();

                    // Close any existing reply forms.
                    $(".block_discussion_feed_reply_form_container").html("");

                    var displayformdata = {
                        courseid: $(event.target).attr('data-course-id'),
                        forumid: $(event.target).attr('data-forum-id'),
                        discussionid: $(event.target).attr('data-discussion-id')
                    };

                    ajax.call([{
                        methodname: 'block_discussion_feed_display_reply_form',
                        args: {'courseid': displayformdata.courseid, 'forumid': displayformdata.forumid,
                            'discussionid': displayformdata.discussionid, 'parentpostid': postid
                        },
                        done:
                            function() {
                                var result = arguments[0];
                                $("#block_discussion_feed_reply_form_container_" + postid).html(result.html);
                                $("#block_discussion_feed_reply_form_container_" + postid +
                                        " #block_discussion_feed_form_message").focus();
                            },

                        fail: function(ex) {
                            $("#comment_" + postid).html('<div class="block_discussion_feed_error">'
                                    + langstrings.errordisplayreplyformtext + '</div>');
                            $("#comment_" + postid).append(langstrings.errortitletext + ' "' + ex.message + '".<br>');
                        }
                    }]);

                    event.preventDefault();
                    event.stopPropagation();
                } else if ((event.target.getAttribute("class") == "block_discussion_feed_action_view_comments_button")) {
                    var discussionidviewbuttonvalue = $(event.target).val();

                    var discussiondata = {
                        courseid: $(event.target).attr('data-course-id'),
                        forumid: $(event.target).attr('data-forum-id'),
                        discussionid: discussionidviewbuttonvalue
                    };

                    displayDiscussion(discussiondata);
                } else if ((event.target.getAttribute("class") == "block_discussion_feed_action_hide_comments_button")) {

                    var hidebuttondiscussionid = $(event.target).val();

                    var hidebuttondata = {
                        courseid: $(event.target).attr('data-course-id'),
                        forumid: $(event.target).attr('data-forum-id'),
                        discussionid: hidebuttondiscussionid
                    };

                    $("#block_discussion_feed_comments_container_" + hidebuttondiscussionid).html('');

                    // Restore the original view comments button.
                    $("#block_discussion_feed_view_comments_button_container_" +
                            hidebuttondiscussionid).html('<button aria-label="comment button" ' +
                            ' class="block_discussion_feed_action_view_comments_button" value="' + hidebuttondiscussionid + '"' +
                            ' data-course-id="' + hidebuttondata.courseid + '" data-forum-id="' +
                            hidebuttondata.forumid + '">' + langstrings.viewcommentstext + '</button>');

                    $("#block_discussion_feed_view_comments_button_container_" + hidebuttondiscussionid + "_second").html('');
                } else if ((event.target.getAttribute("class") == "block_discussion_feed_action_hide_comments_button_second")) {

                    var hidesecondbuttondiscussionid = $(event.target).val();
                    var hidesecondbuttondata = {
                        courseid: $(event.target).attr('data-course-id'),
                        forumid: $(event.target).attr('data-forum-id'),
                        discussionid: hidesecondbuttondiscussionid
                    };

                    $("#block_discussion_feed_comments_container_" + hidesecondbuttondiscussionid).html('');

                    // Restore the original view comments button.
                    $("#block_discussion_feed_view_comments_button_container_" +
                            hidesecondbuttondiscussionid).html('<button aria-label="comment button" ' +
                                            ' class="block_discussion_feed_action_view_comments_button" value="' +
                                            hidesecondbuttondiscussionid + '"' + ' data-course-id="' +
                                            hidesecondbuttondata.courseid + '" data-forum-id="' +
                                            hidesecondbuttondata.forumid + '">' + langstrings.viewcommentstext + '</button>');

                    $("#block_discussion_feed_view_comments_button_container_" + hidesecondbuttondiscussionid + "_second").html('');

                } else if (event.target.getAttribute("class") == 'block_discussion_feed_read_more') {
                    var readmorepostid = $(event.target).attr('data-post-id');
                    $('#post-text-hidden_' + readmorepostid).show();
                    $('#show-less-link_' + readmorepostid).show();
                    $('#post-text-excerpt_' + readmorepostid).hide();
                    $('#read-more-link_' + readmorepostid).hide();
                    event.preventDefault();
                } else if (event.target.getAttribute("class") == 'block_discussion_feed_show_less') {
                    var showlesspostid = $(event.target).attr('data-post-id');
                    $('#post-text-hidden_' + showlesspostid).hide();
                    $('#show-less-link_' + showlesspostid).hide();
                    $('#post-text-excerpt_' + showlesspostid).show();
                    $('#read-more-link_' + showlesspostid).show();
                    event.preventDefault();
                } else if (event.target.getAttribute("id") == 'form-discussion-feed-create-post') {
                    createDiscussion(event);
                    event.stopPropagation();
                    event.preventDefault();
                } else if (event.target.getAttribute("id") == 'form-discussion-feed-create-reply') {
                    createReply(event);
                    event.stopPropagation();
                    event.preventDefault();
                } else if (event.target.getAttribute("id") == 'id_cancel') {
                    var cancelreplybuttondata = $('.block_discussion_feed_reply_form_container #course-discuss-discussion-form')
                        .serializeArray().reduce(function(obj, item) {
                            obj[item.name] = item.value;
                            return obj;
                        }, {});

                    $("#block_discussion_feed_reply_form_container_" + cancelreplybuttondata.parentpostid).html("");
                    $("#comment_" + cancelreplybuttondata.parentpostid).html("");

                    event.stopPropagation();
                }
            });

            $(".block_discussion_feed_expand_container_button").on("click", function(event) {
                if ($('.block_discussion_feed_posts_container_content').hasClass('block_discussion_feed_expand')) {
                    $('.block_discussion_feed_posts_container_content').animate({height: 400}, 200)
                        .removeClass('block_discussion_feed_expand');
                    $('.block_discussion_feed_expand_container_button')
                        .removeClass('block_discussion_feed_expand_container_button_expanded');
                    $('.block_discussion_feed_expand_container_button')
                        .addClass('block_discussion_feed_expand_container_button_collapsed');
                } else {
                    $('.block_discussion_feed_posts_container_content').animate({height: 800}, 200)
                        .addClass('block_discussion_feed_expand');
                    $('.block_discussion_feed_expand_container_button')
                        .removeClass('block_discussion_feed_expand_container_button_collapsed');
                    $('.block_discussion_feed_expand_container_button')
                        .addClass('block_discussion_feed_expand_container_button_expanded');
                }
                event.preventDefault();
            });

            // Handle submit button click on a comment reply form.
            var createReply = function() {

                // Get form data and then post reply.
                var data = $('.block_discussion_feed_reply_form_container #course-discuss-discussion-form').
                    serializeArray().reduce(function(obj, item) {
                        obj[item.name] = item.value;
                        return obj;
                    }, {});

                if (data.message == "") {
                    $(".block_discussion_feed_item_options #comment_" + data.parentpostid)
                        .html('<div class="block_discussion_feed_error">' + langstrings.pleaseaddmessagetext + '</div>');
                    return;
                }

                var result = postReply(data);

                result[0].done(function() {

                    var postresult = arguments[0];
                    if (postresult.eventaction == 'postnotcreated') {
                        $(".block_discussion_feed_item_options #comment_" + data.parentpostid).
                            append('<div class="block_discussion_feed_error">' + langstrings.postcouldnotbeaddedtext
                                    + '</div>');
                        $(".block_discussion_feed_item_options #comment_" + data.parentpostid).append(langstrings.errortitletext
                                + ' "' + postresult.errormessage + '".<br>');
                    } else {

                        $(".block_discussion_feed_item_options #comment_" + data.parentpostid).
                            append('<div class="block_discussion_feed_success">' + langstrings.thankyouforpostingtext + '</div>');

                        $(".block_discussion_feed_item_options #block_discussion_feed_reply_form_container_"
                                + data.parentpostid).html("");

                        $(".block_discussion_feed_item_options #comment_" + data.parentpostid).fadeOut(3000, function() {
                            // Animation complete.
                            $(".block_discussion_feed_item_options #comment_" + data.parentpostid).show();
                            $(".block_discussion_feed_item_options #comment_" + data.parentpostid).text("");

                            displayDiscussion(data);

                            $("#block_discussion_feed_view_comments_button_container_" +
                                    data.discussionid).html('<button aria-label="comment button" ' +
                                    ' class="block_discussion_feed_action_view_comments_button" value="' +
                                    data.discussionid + '"' + ' data-course-id="' + data.courseid +
                                    '" data-forum-id="' + data.forumid + '">' +
                                    langstrings.viewcommentstext + '</button>');

                            $("#discussion-feed-no-comments-container_" + data.discussionid).html('');
                        });

                    }

                }).fail(function(ex) {
                    $(".block_discussion_feed_item_options #comment_" + data.parentpostid)
                        .append('<div class="block_discussion_feed_error">'
                            + langstrings.postcouldnotbeaddedtext + '</div>');
                    $(".block_discussion_feed_item_options #comment_" + data.parentpostid).append(langstrings.errortitletext
                            + ' "' + ex.message + '".<br>');
                });
            };

            // Handler to create a new discussion for a forum.
            var createDiscussion = function() {
                var data = $('#block_discussion_feed_new_discussion_container #block_discussion_feed_discussion_form')
                    .serializeArray().reduce(function(obj, item) {
                        obj[item.name] = item.value;
                        return obj;
                    }, {});

                if (data.subject == "") {
                    $(".block_discussion_feed_posts_container_content #comment_0").html('<div class="block_discussion_feed_error">'
                            + langstrings.pleaseaddsubjecttext + '</div>');
                    return;
                }
                if (data.message == "") {
                    $(".block_discussion_feed_posts_container_content #comment_0").html('<div class="block_discussion_feed_error">'
                            + langstrings.pleaseaddmessagetext + '</div>');
                    return;
                }

                var groupid = -1;
                if (data.group) {
                    groupid = data.group;
                }

                ajax.call([{
                    methodname: 'block_discussion_feed_create_discussion',
                    args: {'forumid': data.forumid,
                           'subject': data.subject, 'message': data.message,
                           'groupid': groupid
                    },
                    done:

                    function() {

                        var result = arguments[0];
                        if (result.discussionid > 0) {
                            $(".block_discussion_feed_posts_container_content #comment_0").append('<div '
                                    + 'class="block_discussion_feed_success">' + langstrings.thankyouforpostingtext
                                    + '</div>');

                             $(".block_discussion_feed_posts_container_content #comment_0").fadeOut(3000, function() {
                                 // Animation complete.
                                 $(".block_discussion_feed_posts_container_content #comment_0").show();
                                 $(".block_discussion_feed_posts_container_content #comment_0").text("");

                                 var textMax = 1200;
                                 $('.block_discussion_feed_posts_container_content #block_discussion_feed_form_message_feedback')
                                     .html(textMax + langstrings.charactersremainingtext);

                                 displayAllDiscussions(data);
                             });
                        } else {
                            $(".block_discussion_feed_posts_container_content #comment_0")
                                .html('<div class="block_discussion_feed_error">'
                                    + langstrings.postcouldnotbeaddedtext + '</div>');
                            $(".block_discussion_feed_posts_container_contents #comment_0").append(langstrings.errortitletext
                                    + ' "' + result.warnings + '".<br>');
                        }
                    },

                    fail: function(ex) {
                        $(".block_discussion_feed_posts_container_content #comment_0")
                            .html('<div class="block_discussion_feed_error">'
                                + langstrings.postcouldnotbeaddedtext + '</div>');
                        $(".block_discussion_feed_posts_container_content #comment_0").append(langstrings.errortitletext + ' "'
                                + ex.message + '".<br>');
                    }
                }]);

            };

            /**
             * Display all discussions.
             *
             * @method displayAllDiscussions
             * @param {Object} data Contains data required.
             * @return {Promise} jQuery Deferred object to fail or resolve
             */
            var displayAllDiscussions = function(data) {

                return ajax.call([{
                    methodname: 'block_discussion_feed_display_all_discussions',
                    args: {'courseid': data.courseid, 'forumid': data.forumid,
                        'shownewdiscussionform': true},
                    done: function() {
                        var result = arguments[0];

                        var displaycontent = "";

                        displaycontent = result.html;
                        $(".block_discussion_feed_posts_container_content").html(displaycontent);

                    },

                    fail: function(ex) {
                        $(".block_discussion_feed_posts_container_content").html('<div class="block_discussion_feed_error">'
                                + langstrings.errordisplayingdiscussiontext + '</div>');
                        $(".block_discussion_feed_posts_container_content").append(langstrings.errortitletext + ' "'
                                + ex.message + '".<br>');
                    }
                }]);

            };

            /**
             * Display the discussion.
             *
             * @method displayDiscussion
             * @param {Object} data Contains data required.
             * @return {Promise} jQuery Deferred object to fail or resolve
             */
            var displayDiscussion = function(data) {

                return ajax.call([{
                    methodname: 'block_discussion_feed_display_discussion_posts',
                    args: {'courseid': data.courseid, 'forumid': data.forumid, 'discussionid': data.discussionid},
                    done: function() {
                        var result = arguments[0];

                        var displaycontent = "";
                        if (data.usermessage) {
                            displaycontent = data.usermessage;
                        }

                        displaycontent += result.html;
                        $("#block_discussion_feed_comments_container_" + data.discussionid).html(displaycontent);

                        $("#block_discussion_feed_view_comments_button_container_" +
                                data.discussionid).html('<button aria-label="comment button" ' +
                                'class="block_discussion_feed_action_hide_comments_button" value="' +
                                data.discussionid + '"' + 'data-course-id="' + data.courseid +
                                '" data-forum-id="' + data.forumid + '">' + langstrings.hidecommentstext + '</button>');

                        $("#block_discussion_feed_view_comments_button_container_" + data.discussionid +
                                '_second').html('<button aria-label="comment button" ' +
                                'class="block_discussion_feed_action_hide_comments_button_second" value="'
                                        + data.discussionid + '"' + 'data-course-id="' + data.courseid + '" data-forum-id="'
                                        + data.forumid + '">' + langstrings.hidetext + '</button>');

                    },

                    fail: function(ex) {
                        $("#block_discussion_feed_comments_container_" + data.discussionid)
                            .html('<div class="block_discussion_feed_error">'
                                + langstrings.errordisplayingdiscussiontext + '</div>');
                        $("#block_discussion_feed_comments_container_" + data.discussionid).append(langstrings.errortitletext
                                + ' "' + ex.message + '".<br>');

                    }
                }]);

            };

            /**
             * Post a reply.
             *
             * @method postReply
             * @param  {Object} data Contains required data,
             * @return {Promise} jQuery Deferred object
             */
            var postReply = function(data) {

                return ajax.call([{
                    methodname: 'block_course_discuss_create_post',
                    args: {'courseid': data.courseid, 'forumid': data.forumid, 'contextid': data.contextid,
                        'message': data.message, 'discussionid': data.discussionid, 'subject': data.subject,
                        'parentpostid': data.parentpostid}
                }]);

            };

        }

    };
});
