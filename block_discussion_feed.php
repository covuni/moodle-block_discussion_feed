<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle Discussion Feed block.
 *
 * Aggregates and displays the latest discussions from all forums the Moodle user has access to,
 * mainly through enrolled courses.  In addition, it allows the user to reply to posts as appropriate.
 * The block is designed to only appear on the dashboard page, front page and course homepages.  As such,
 * it requires adding to a block region that appears on these pages.
 *
 * @package block_discussion_feed
 * @copyright 2018 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->libdir . '/externallib.php');

require_once(dirname(__FILE__) . '/lib.php');

/**
 * Discussion feed block implementation class.
 *
 * @package   block_discussion_feed
 * @copyright 2018 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_discussion_feed extends block_base {

    /** @var array The plugin name of the block */
    const BLOCKPLUGINNAME = 'block_discussion_feed';

    /**
     * Adds title to block instance.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_discussion_feed');
    }

    /**
     * Set up any configuration data.
     *
     * The is called immediatly after init().
     */
    public function specialization() {
        $config = get_config("block_discussion_feed");

        // Use the title as defined in plugin settings, if one exists.
        if (!empty($config->title)) {
            $this->title = $config->title;
        } else {
            $this->title = get_string('pluginname', 'block_discussion_feed');
        }
    }

    /**
     * Gets Javascript that may be required for navigation.
     */
    public function get_required_javascript() {
        parent::get_required_javascript();

        // We need to get the language strings and store in an array.  Ideally we could use the language string manager class which
        // gets all of them at once but due to a Moodle core library issue we just do it manually.
        // The js_call_amd function displays a debug message when the params total length is over
        // a certain length at present. See here: https://tracker.moodle.org/browse/MDL-62468.

        $requiredparams = array ('viewcommentstext' => get_string('viewcommentstext', 'block_discussion_feed'),
                                 'hidecommentstext' => get_string('hidecommentstext', 'block_discussion_feed'),
                                 'hidetext' => get_string('hidetext', 'block_discussion_feed'),
                                 'charactersremainingtext' => get_string('charactersremainingtext',
                                         'block_discussion_feed'),
                                 'errortitletext' => get_string('errortitletext', 'block_discussion_feed'),
                                 'errordisplayreplyformtext' => get_string('errordisplayreplyformtext',
                                         'block_discussion_feed'),
                                 'postcouldnotbeaddedtext' => get_string('postcouldnotbeaddedtext',
                                         'block_discussion_feed'),
                                 'pleaseaddmessagetext' => get_string('pleaseaddmessagetext',
                                         'block_discussion_feed'),
                                 'thankyouforpostingtext' => get_string('thankyouforpostingtext', 'block_discussion_feed'),
                                 'pleaseaddsubjecttext' => get_string('pleaseaddsubjecttext', 'block_discussion_feed'),
                                 'errordisplayingdiscussiontext' => get_string('errordisplayingdiscussiontext',
                                         'block_discussion_feed')
                                );
        $this->page->requires->js_call_amd('block_discussion_feed/discussion_feed', 'init', array($requiredparams));
    }

    /**
     * Which page types this block may appear on.  This currently is set to allow it to display
     * on the front page, dashboard page and the course home page.
     */
    public function applicable_formats() {
        return array('site-index' => true, 'my' => true, 'course-view-*' => true);
    }

    /**
     * Get block instance content.
     */
    public function get_content() {
        global $CFG, $COURSE, $USER, $OUTPUT, $PAGE, $ME, $DB;

        if ($this->content !== null) {
            return $this->content;
        }

        $config = get_config("block_discussion_feed");

        if (!isloggedin()) {
            return $this->content;
        }

        // Number of days in the past to get discussions for.
        if (!empty ($config->discussionagedays)) {
            $discussionagedays = $config->discussionagedays;
        } else {
            $discussionagedays = DISCUSSION_FEED_DISCUSSION_AGE_DAYS_DEFAULT;
        }

        // Check page type.  This can display only on dashboard, frontpage or a course homepage.
        $currentpage = block_discussion_feed_get_current_page();

        // If empty, then means we're not on a page where this block should be displayed.
        if (empty($currentpage)) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';

        $showexpand = false;
        if (!empty ($config->showexpandtoggle)) {
            $showexpand = true;
        }

        $extraclasses = '';
        if ($showexpand == false) {
            $extraclasses .= ' block_discussion_feed_expand ';
        }

        $blockheader = '<h3 class="headline block_discussion_feed_headline">' .
                       get_string('blockheadertitle', self::BLOCKPLUGINNAME) . '</h3>' .
                       '<div class="block_discussion_feed_container"><div class="block_discussion_feed_posts_container_content '
                       . $extraclasses . '">';

        $blockfooter = '</div><div class="block_discussion_feed_expand_container">';

        if ($showexpand == true) {
            $blockfooter .= '<button class="block_discussion_feed_expand_container_button ' .
                            ' block_discussion_feed_expand_container_button_collapsed"></button>';
        }
        $blockfooter .= '</div></div>'; // End main container.

        // If we're on a course home page, check for a relevant forum id for the course.
        if ($currentpage == 'coursepage') {

            // Find a valid discussion forum for the course.
            $forumid = block_course_discuss_get_forum_id_for_course($COURSE->id, MUST_EXIST);

            if (!$forumid) {

                // Print a message when there is no valid forum.
                $this->content->text .= get_string('displaynoforumexistsmessagetext', 'block_discussion_feed');
                return $this->content->text;

            } else {

                $courses = array();
                $forum = block_discussion_get_forum_from_id($forumid, MUST_EXIST);
                $courses[$forum->id] = $COURSE;

                // Provide a link to the forum object.  This will be used later to check if user has permission to post etc.
                $courses[$forum->id]->forum = $forum;
                $cm = get_coursemodule_from_instance('forum', $forumid, $COURSE->id, false, MUST_EXIST);

                // Display comment form if appropriate permissions for the course.
                $currentgroup = -1;
                $groupmode = -1;
                $context = context_module::instance($cm->id);

                // First check the group stuff
                if ($currentgroup == -1 or $groupmode == -1) {
                    $groupmode    = groups_get_activity_groupmode($cm, $COURSE);
                    $currentgroup = groups_get_activity_group($cm);
                }

                // The following is taken from forum/lib.php, forum_print_latest_discussions().
                $canpost = forum_user_can_post_discussion($forum, $currentgroup, $groupmode, $cm, $context);
                if (!$canpost and $forum->type !== 'news') {
                    if (isguestuser() or !isloggedin()) {
                        $canpost = true;
                    }
                    if (!is_enrolled($context) and !is_viewing($context)) {
                        // allow guests and not-logged-in to see the button - they are prompted to log in after clicking the link
                        // normal users with temporary guest access see this button too, they are asked to enrol instead
                        // do not show the button to users with suspended enrolments here
                        $canpost = enrol_selfenrol_available($COURSE->id);
                    }
                }

                if ($canpost) {
                    $this->content->text .= $blockheader;

                    // Display form for a new discussion.
                    $discussionid = 0;
                    $parentpostid = 0;
                    $this->content->text .= '<div id="block_discussion_feed_new_discussion_container">';
                    $this->content->text .= block_discussion_feed_display_new_discussion_form($cm, $forum, $COURSE,
                                            'discussion-feed-create-post');
                    $this->content->text .= '</div><div id="comment_0"></div>';
                }
            }

            // Get the latest posts anyway to allow replies.
            $latestposts = block_discussion_feed_get_latest_discussions($courses, $discussionagedays);
            $this->content->text .= '<div class="block_discussion_feed_container">';
            $this->content->text .= $latestposts;
            $this->content->text .= '</div>';
            $this->content->text .= $blockfooter;

        } else {

            // Ensure we only get forum updates for visible courses.

            // Check if caching is being used.  If so, get user's list of courses from cache.
            if (!empty ($config->usecaching)) {

                $cache = cache::make(self::BLOCKPLUGINNAME, DISCUSSION_FEED_CACHE_MY_COURSES_DATA);
                $returnedcachedata = $cache->get(DISCUSSION_FEED_CACHE_MY_COURSES_KEY);

                $cachedatastore = array();

                $usercachettl = $config->cachingttl;
                $timenow = time();

                // If no data retrieved or lastcachebuildtime has no value.
                // Or if user's last cache has expired since it was last built.
                if ( ($returnedcachedata === false) || (!isset($returnedcachedata['lastcachebuildtime'])) ||
                    ( $timenow > ($returnedcachedata['lastcachebuildtime'] + $usercachettl)) ) {
                        $cachedatastore['data'] = enrol_get_my_courses('id, shortname', 'visible DESC, sortorder ASC');

                        // Now timestamp the cache with last build time.
                        $cachedatastore['lastcachebuildtime'] = time();
                        $cache->set(DISCUSSION_FEED_CACHE_MY_COURSES_KEY, $cachedatastore);
                } else {
                    $cachedatastore['data'] = $returnedcachedata['data'];  // We got valid, non-expired data from cache.
                }

                $courses = $cachedatastore['data'];

            } else {
                $courses = enrol_get_my_courses('id, shortname', 'visible DESC, sortorder ASC');
            }

            if (empty ($courses)) {
                // If the user is not enrolled on any courses, then we do not need to do anything.

                // Print a message when there is no content.
                $this->content->text .= get_string('nocontentdisplaymessagetext', 'block_discussion_feed');

                return $this->content->text;
            }

            // Now get the latest posts for all user's courses.
            $latestposts = block_discussion_feed_get_latest_discussions($courses, $discussionagedays);

            if (!empty ($latestposts)) {
                $this->content->text .= $blockheader;
                $this->content->text .= '<div class="block_discussion_feed_container">';
                $latestposts = block_discussion_feed_get_latest_discussions($courses, $discussionagedays);
                $this->content->text .= '</div>';
                $this->content->text .= $latestposts;
                $this->content->text .= $blockfooter;
            } else {
                // Print a message when there is no content.
                $this->content->text .= get_string('nocontentdisplaymessagetext', 'block_discussion_feed');
            }
        }

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Allows multiple instances of the block.
     */
    public function instance_allow_multiple() {
        return true;
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        $config = get_config("block_discussion_feed");

        // If title in settings is empty, hide header.
        if (!empty($config->title)) {
            return false;
        } else {
            return true;
        }
    }

}
