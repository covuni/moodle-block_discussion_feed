<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the web service calls utilised by the block to perform the required functions.
 *
 * @package    block_discussion_feed
 * @copyright  2018 Coventry University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Manoj Solanki (Coventry University)
 */

defined('MOODLE_INTERNAL') || die();

$functions = array(
    'block_discussion_feed_display_discussion_posts' => array(
        'classname'   => 'block_discussion_feed_external',
        'methodname'  => 'display_discussion_posts',
        'classpath'   => 'blocks/discussion_feed/externallib.php',
        'description' => 'Display a discussion.',
        'type'        => 'read',
        'ajax'          => true,
        'loginrequired' => true
    ),
    'block_discussion_feed_create_discussion' => array(
        'classname'   => 'block_discussion_feed_external',
        'methodname'  => 'create_discussion',
        'classpath'   => 'blocks/discussion_feed/externallib.php',
        'description' => 'Create a discussion.',
        'type'        => 'read',
        'ajax'          => true,
        'loginrequired' => true
    ),
    'block_discussion_feed_display_all_discussions' => array(
        'classname'   => 'block_discussion_feed_external',
        'methodname'  => 'display_all_discussions',
        'classpath'   => 'blocks/discussion_feed/externallib.php',
        'description' => 'Display all discussions.',
        'type'        => 'read',
        'ajax'          => true,
        'loginrequired' => true
    ),
    'block_discussion_feed_display_reply_form' => array(
        'classname'   => 'block_discussion_feed_external',
        'methodname'  => 'display_reply_form',
        'classpath'   => 'blocks/discussion_feed/externallib.php',
        'description' => 'Display a reply form.',
        'type'        => 'read',
        'ajax'          => true,
        'loginrequired' => true
    )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
    'Discussion Feed' => array(
        'functions' => array ('block_discussion_feed_display_discussion_posts', 'block_discussion_feed_create_discussion',
                              'block_discussion_feed_display_reply_form', 'block_discussion_feed_display_all_discussions'),
        'restrictedusers' => 0,
        'enabled' => 1,
    )
);
