<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Discussion Feed block helper functions and callbacks.
 *
 * @package block_discussion_feed
 * @copyright 2018 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/forum/lib.php');
require_once($CFG->dirroot . '/mod/forum/externallib.php');
require_once($CFG->dirroot . '/blocks/course_discuss/lib.php');
require_once($CFG->dirroot . '/blocks/course_discuss/lib.php');

define( 'DISCUSSION_FEED_DISCUSSION_AGE_DAYS_DEFAULT', 30);
define( 'DISCUSSION_FEED_CACHE_TTL_DEFAULT', 300);

// Cache names.
define( 'DISCUSSION_FEED_CACHE_MY_COURSES_DATA', 'discussionfeedcachemycoursesdata');
define( 'DISCUSSION_FEED_CACHE_MY_COURSES_KEY', 'discussionfeedcachemycourseskey');
define( 'DISCUSSION_FEED_CACHE_FORUMS_FOR_COURSES_DATA', 'discussionfeedcacheforumsforcoursesdata');
define( 'DISCUSSION_FEED_CACHE_FORUMS_FOR_COURSES_KEY', 'discussionfeedcacheforumsforcourseskey');
define( 'DISCUSSION_FEED_CACHE_DISCUSSIONS_FOR_FORUMS_DATA', 'discussionfeedcachediscussionsforforumsdata');
define( 'DISCUSSION_FEED_CACHE_DISCUSSIONS_FOR_FORUMS_KEY', 'discussionfeedcachediscussionsforforumskey');

// These are required for library function called block_discussion_feed_time_ago, to get time elapsed since a post was made.
define( 'DISCUSSION_FEED_TIMEBEFORE_NOW', 'now' );
define( 'DISCUSSION_FEED_TIMEBEFORE_MINUTE', '{num} minute ago' );
define( 'DISCUSSION_FEED_TIMEBEFORE_MINUTES', '{num} minutes ago' );
define( 'DISCUSSION_FEED_TIMEBEFORE_HOUR', '{num} hour ago' );
define( 'DISCUSSION_FEED_TIMEBEFORE_HOURS', '{num} hours ago' );
define( 'DISCUSSION_FEED_TIMEBEFORE_YESTERDAY', 'yesterday' );
define( 'DISCUSSION_FEED_TIMEBEFORE_FORMAT', '%e %b' );
define( 'DISCUSSION_FEED_TIMEBEFORE_FORMAT_YEAR', '%e %b, %Y' );

/**
 *
 * Get the current page to allow us to check if the block is allowed to display.
 *
 * @return string The page name, which is either "frontpage", "dashboard" or "coursepage", or empty string.
 *
 */
function block_discussion_feed_get_current_page() {
    global $COURSE, $PAGE;

    // This will store the kind of activity page type we find. E.g. It will get populated with 'section' or similar.
    $currentpage = '';

    // We expect $PAGE->url to exist.  It should!
    $url = $PAGE->url;

    if ($PAGE->pagetype == 'site-index') {
        $currentpage = 'frontpage';
    } else if ($PAGE->pagetype == 'my-index') {
        $currentpage = 'dashboard';
    }

    // Check if course home page.
    if (empty ($currentpage)) {
        if ($url !== null) {
            // Check if this is the course view page.
            if (strstr ($url->raw_out(), 'course/view.php')) {

                // Get raw querystring params from URL.
                $getparams = http_build_query($_GET);

                // Check url paramaters.  Count should be 1 if course home page.
                // Checking that section param doesn't exist as an extra if relevant.  Also checking raw querystring defined
                // above.  This is due to section 0 not actually recording 'section' as a param.
                $urlparams = $url->params();

                $config = get_config('block_discussion_feed');

                // Allow the block to display on course sections too if the relevant setting is on.
                if (!empty ($config->displayinsections)) {
                    if ((count ($urlparams) == 1) || (array_key_exists('section', $urlparams))) {
                        $currentpage = 'coursepage';
                    }
                } else {
                    if ((count ($urlparams) == 1) && (!array_key_exists('section', $urlparams))) {
                        $currentpage = 'coursepage';
                    }
                }

            }
        }
    }

    return $currentpage;
}

/**
 * Get all forums for the course.  Based on forum_get_course_forum().  This can filter out any unrequired
 * forums.
 *
 * @param array $courseids Array of course ids
 * @param array $userid User ids
 *
 * @return string Returns all forums for the courses specified
 */
function block_discussion_feed_get_forums_for_courses($courseids, $userid) {

    global $DB;
    static $forumslist = array();

    $config = get_config('block_discussion_feed');

    $currentpagetype = block_discussion_feed_get_current_page();

    if ( ($currentpagetype == 'dashboard') && (!empty ($config->excludeforumids) ) ) {
        // This may contain a comma separated list of strings.  So we treat as an array.
        $excludeforumids = explode(',', $config->excludeforumids);
    } else {
        $excludeforumids = array();
    }

    if (empty($forumslist[$userid])) {
        list($inclause, $params) = $DB->get_in_or_equal($courseids, SQL_PARAMS_NAMED);
        if ($forums = $DB->get_records_select("forum", "type <> 'news' and course $inclause", $params, "id ASC")) {
            $forumslisttemp = array();
            foreach ($forums as $forum) {

                // Check if this forum should be excluded.
                if (!empty ($excludeforumids)) {
                    if (in_array ($forum->id, $excludeforumids)) {
                        continue;
                    }
                }

                $forumslisttemp[] = $forum;
            }
            $forumslist[$userid] = $forumslisttemp;
        } else {
            $forumslist[$userid] = array();
        }

    }
    return $forumslist[$userid];

}

/**
 * Get latest posts for the courses specified. Be wary of adding echo statements here as
 * this method is called by the web service function display_all_discussions.
 *
 * @param object   $courses         Array of course objects
 * @param int      $timespanindays  Time span in the past for which to retrieve posts
 *
 *
 * @return string Return discussion content.
 */
function block_discussion_feed_get_latest_discussions($courses, $timespanindays = 30) {
    global $CFG, $PAGE, $OUTPUT, $USER, $DB;

    $content = '';

    // Use this variable to store multiple forum activities.
    $forumsfoundarray = array();

    $courseids = array();

    foreach ($courses as $course) {
        array_push($courseids, $course->id);
    }

    // Get all allowed forums for the course. This is cached if it's on.
    // Check if caching is being used.  If so, get user's list of courses from cache.

    $config = get_config('block_discussion_feed');
    if (!empty ($config->usecaching)) {

        $cache = cache::make('block_discussion_feed', DISCUSSION_FEED_CACHE_FORUMS_FOR_COURSES_DATA);
        $returnedcachedata = $cache->get(DISCUSSION_FEED_CACHE_FORUMS_FOR_COURSES_KEY);

        // Use this to write data to cache in array format, ['lastcachebuildtime'] = last access time, ['data'] = actual data.
        $cachedatastore = array();

        $usercachettl = $config->cachingttl;
        $timenow = time();

        // If no data retrieved or lastcachebuildtime has no value.
        // Or if user's last cache has expired since it was last built.
        if ( ($returnedcachedata === false) || (!isset($returnedcachedata['lastcachebuildtime'])) ||
            ( $timenow > ($returnedcachedata['lastcachebuildtime'] + $usercachettl)) ) {

                $cachedatastore['data'] = block_discussion_feed_get_forums_for_courses($courseids, $USER->id);

                // Now timestamp the cache with last build time.
                $cachedatastore['lastcachebuildtime'] = time();
                $cache->set(DISCUSSION_FEED_CACHE_FORUMS_FOR_COURSES_KEY, $cachedatastore);

        } else {
            $cachedatastore['data'] = $returnedcachedata['data'];  // We got valid, non-expired data from cache.
        }

        $forumsforcourses = $cachedatastore['data'];

    } else {
        $forumsforcourses = block_discussion_feed_get_forums_for_courses($courseids, $USER->id);
    }

    foreach ($courses as $course) {
        $modinfo = get_fast_modinfo($course);
        $modfullnames = array();

        foreach ($modinfo->cms as $cm) {
            if ($cm->modname == 'forum') {

                $forumid = array_search($cm->instance, array_column($forumsforcourses, 'id'));
                if ($forumid !== false) {

                    $modcontext = context_module::instance($cm->id);

                    // Finally check that the module is visible.
                    if ($cm->uservisible) {
                        $forumsfoundarray[$cm->id]['cm'] = $cm;
                        $forumsfoundarray[$cm->id]['course'] = $course;

                    }
                }
            }
        }
    }

    $forumtracked = false;
    $allowreply = false;

    $discussionsfound = array();

    // Go through all forums, getting the discussions by last update date.  This could be
    // quite resource intensive, so may need to think about the SQL that is run and possible
    // optimisation.
    foreach ($forumsfoundarray as $currentforumcm) {
        $sort = forum_get_default_sort_order();

        $updatedsince = (time() - (86400 * $timespanindays));

        $config = get_config('block_discussion_feed');

        $discussions = forum_get_discussions($currentforumcm['cm'], $sort, true, -1, -1, false, -1, 0,
                        FORUM_POSTS_ALL_USER_GROUPS, $updatedsince);

        // Get forum object. Used to check if user has permission to reply.
        // Use ignore missing strictness incase forum doesn't exist for
        // any reason.
        $forum = block_discussion_get_forum_from_id($currentforumcm['cm']->instance, IGNORE_MISSING);

        foreach ($discussions as $discussion => $value) {
            $value->course = $currentforumcm['course'];
            $value->forumid = $currentforumcm['cm']->instance;
            $value->forumname = $currentforumcm['cm']->name;
            $value->forum = $forum;
            $modcontext = context_module::instance($currentforumcm['cm']->id);
            $value->canedit = has_capability('mod/forum:editanypost', $modcontext);

            $value->cm = $currentforumcm['cm'];

            // Check if user can reply to a post.
            $value->canreply = false;
            if ($value->forum) {
                $value->canreply = forum_user_can_post($value->forum, $value, $USER,
                                   $currentforumcm['cm'], $currentforumcm['course'], $modcontext);
            }
            $discussionsfound[] = $value;

        }

    }

    $listofposts = array();

    $currentpagetype = block_discussion_feed_get_current_page();

    foreach ($discussionsfound as $discussion) {

        $editlink = '';
        $editpostallowed = false;

        $age = time() - $discussion->created;

        if (isloggedin()) {
            $ownpost = ($discussion->userid == $USER->id);
        } else {
            $ownpost = false;
        }

        // This is always expected to find the discussion forum.
        $forumid = array_search($discussion->forumid, array_column($forumsforcourses, 'id'));
        if ($forumid !== false) {
            if (!$discussion->parent && $forumsforcourses[$forumid]->type == 'news' && $discussion->timestart > time()) {
                $age = 0;
            }

            if (($ownpost && $age < $CFG->maxeditingtime) || $discussion->canedit) {
                $editlink = new moodle_url('/mod/forum/post.php', array('edit' => $discussion->id));
                $editpostallowed = true;
            }
        }

        $groupname = '';
        if (!empty($discussion->cm->groupmode)) {
            $group = groups_get_group($discussion->groupid);
            if ($group) {
                $groupname = $group->name;
            }
        }

        // Add all post related fields to a post variable.
        $listofposts[] = (object) array ('id' => $discussion->id,
            'discussion' => $discussion->discussion,
            'lastmodified' => $discussion->timemodified,
            'parent' => $discussion->parent,
            'userid' => $discussion->userid,
            'created' => $discussion->created,
            'modified' => $discussion->modified,
            'mailed' => $discussion->mailed,
            'subject' => clean_text($discussion->subject), // By default we clean this and next parament of any html / scripts.
            'message' => clean_text($discussion->message),
            'messageformat' => $discussion->messageformat,
            'attachment' => $discussion->attachment,
            'picture' => $discussion->picture,
            'imagealt' => $discussion->imagealt,
            'email' => $discussion->email,
            'firstname' => $discussion->firstname,
            'lastname' => $discussion->lastname,
            'shortname' => $discussion->course->shortname,
            'courselink' => new moodle_url('/course/view.php?id=' . $discussion->course->id),
            'forumlink' => new moodle_url('/mod/forum/view.php?f=' . $discussion->forumid),
            'forumname' => $discussion->forumname,
            'editlink' => $editlink,
            'editlinktext' => get_string('editlinktext', 'block_discussion_feed'),
            'editpostallowed' => $editpostallowed,
            'canreply' => $discussion->canreply,
            'threadlink' => new moodle_url('/mod/forum/discuss.php', ['d' => $discussion->discussion]),
            'threadlinktext' => get_string('threadlinktext', 'block_discussion_feed'),
            'thediscussion' => $discussion,
            'postedintext' => get_string('postedintext', 'block_discussion_feed'),
            'coursetext' => get_string('coursetext', 'block_discussion_feed'),
            'lastupdatedtext' => get_string('lastupdatedtext', 'block_discussion_feed'),
            'viewcommentstext' => get_string('viewcommentstext', 'block_discussion_feed'),
            'nocommentstext' => get_string('nocommentstext', 'block_discussion_feed'),
            'groupname' => $groupname,
            'replytext' => get_string('replytext', 'block_discussion_feed'),
            'pinned' => ($currentpagetype == "coursepage") ? $discussion->pinned : 0,
        );

    }

    if ($currentpagetype == "coursepage") {
        // Order all posts by date and pinned status now.
        array_multisort(array_column($listofposts, 'pinned'),  SORT_DESC,
                array_column($listofposts, 'lastmodified'), SORT_DESC,
                $listofposts);
    } else {
        // Order all posts by date now.
        usort($listofposts, function ($a, $b) {
            return $a->lastmodified < $b->lastmodified;
        });
    }

    foreach ($listofposts as $thepost => $post) {
        $content .= block_discussion_feed_render_primary_post($post->thediscussion->cm, $post->thediscussion, $post);
    }

    return $content;
}


/**
 * Get discussion posts for the specified discussion.
 *
 * @param object   $cm              Relevant course module
 * @param object   $course          Course object
 * @param object   $discussion      Discussion object
 * @param object   $forum           Forum object
 *
 * @return string Return discussion content.
 */
function block_discussion_feed_display_discussion($cm, $course, $discussion, $forum) {
    global $CFG, $PAGE, $OUTPUT, $USER;

    // Get the discussion.
    $sort = "p.created DESC";
    $forumtracked = false;

    $parent = $discussion->firstpost;
    $post = forum_get_post_full($parent);

    $modcontext = context_module::instance($cm->id);

    $allowreply = false;

    $posts = forum_get_all_discussion_posts($discussion->id, $sort, $forumtracked);
    $post = $posts[$post->id];

    if (!empty($posts)) {

        // Get all posts in a nested fashion.

        $content .= '<ul class="block_discussion_feed_posts_container">';

        // Check if user can reply to a post.
        $canreply = forum_user_can_post($forum, $discussion, $USER, $cm, $course, $modcontext);

        ob_start();
        block_discussion_feed_get_posts_nested($course, $cm, $forum, $discussion, $post, $forumtracked, $posts, $canreply);
        $postscontent = ob_get_contents();
        ob_end_clean();
        $content .= $postscontent;
        $content .= '</ul>';

    } else {
        $content .= 'No posts found';
    }

    return $content;
}

/**
 * Render the main post for the discussion.
 *
 * @param stdClass $cm              Relevant course module
 * @param stdClass $discussion      Discussion object
 * @param stdClass $post            Post object
 *
 * @return string Return discussion content in HTML.
 */
function block_discussion_feed_render_primary_post($cm, $discussion, $post) {
    global $USER, $CFG, $OUTPUT;

    // User picture.
    $postuser = new stdClass();
    $postuserfields = explode(',', user_picture::fields());
    $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
    $postuser->id = $post->userid;

    $post->userpicture = $OUTPUT->user_picture($postuser, array('courseid' => $discussion->course->id));

    $modcontext = context_module::instance($cm->id);

    // User name.
    $post->userfullname = fullname($postuser, has_capability('moodle/site:viewfullnames', $modcontext));
    $post->userprofilelink = new moodle_url($CFG->wwwroot . '/user/view.php?id=' . $post->userid . '&amp;course=' .
                             $discussion->course->id);
    $post->discussionid = $discussion->discussion;

    // Time since post.
    $post->timesince = block_course_discuss_time_ago($discussion->timemodified);
    $post->timecreated = date( "h:iA, l, d M Y ", $post->created);

    $post->courseid = $discussion->course->id;
    $post->forumid = $discussion->forumid;

    $post->postid = $post->id;

    // This will allow us to not display view comments link if one is not available.
    $post->singlepostonly = false;
    $post->multiplepostsmade = false;

    if ($discussion->created == $discussion->timemodified) {
        $post->singlepostonly = true;
    } else {
        $post->multiplepostsmade = true;
    }

    $postread = !empty($post->postread);

    $config = get_config('block_discussion_feed');
    $maxpostlengthtodisplay = 0;

    if (!empty ($config->maxpostlengthtodisplay)) {
        $maxpostlengthtodisplay = $config->maxpostlengthtodisplay;
    }

    // Display excerpt of post if necessary.
    if ($maxpostlengthtodisplay) {
        $messagelength = strlen($post->message);
        $originalmessage = $post->message;
        if (strlen($post->message) > $maxpostlengthtodisplay) {

            $excerpt = mb_strimwidth(format_string($post->message), 0, $maxpostlengthtodisplay, '...', 'utf-8');

            $fullmessage = $post->message;

            $readmoretext = get_string('readmoretext', 'block_discussion_feed');
            $showlesstext = get_string('showlesstext', 'block_discussion_feed');

            $showlesslink = '<a href="#" ' .
                ' title="' . $showlesstext . '" class="block_discussion_feed_show_less" ' .
                ' id="show-less-link_' . $post->id . '" ' . ' data-post-id="' .
                $post->id . '">' . $showlesstext . '</a>';

                $post->message = '<div id="post-text-excerpt_' . $post->id . '">' . $excerpt . ' <a href="#" ' .
                    ' title="' . $readmoretext . '" class="block_discussion_feed_read_more" id="read-more-link_' .
                    $post->id . '" ' . ' data-post-id="' . $post->id . '">' . $readmoretext . '</a></div>' .
                    '<div id="post-text-hidden_' . $post->id .
                    '" class="block_discussion_feed_hidden">' . $fullmessage . '</div><br>' . $showlesslink;
        }

    }

    $discussionpostsjson = new stdClass();
    $discussionpostsjson = $post;

    return $OUTPUT->render_from_template('block_discussion_feed/discussion-primary-post', $discussionpostsjson);

}

/**
 * Process all posts for a discussion and return in HTML.
 *
 * @param stdClass $course          Relevant course
 * @param stdClass $cm              Relevant course module
 * @param stdClass $forum           Forum object
 * @param stdClass $discussion      Discussion object
 * @param stdClass $parent          Parent post object
 * @param bool     $forumtracked    Is forum tracked
 * @param array    $posts           array of post objects
 * @param bool     $canreply        Can user reply to posts
 *
 * @return string Return discussion content in HTML.
 */
function block_discussion_feed_get_posts_nested($course, $cm, $forum, $discussion, $parent, $forumtracked, $posts,
                                                $canreply = false) {
    global $USER, $CFG, $OUTPUT;

    $config = get_config('block_discussion_feed');
    $maxpostlengthtodisplay = 0;

    if (!empty ($config->maxpostlengthtodisplay)) {
        $maxpostlengthtodisplay = $config->maxpostlengthtodisplay;
    }

    if (!empty($posts[$parent->id]->children)) {
        $posts = $posts[$parent->id]->children;

        foreach ($posts as $post) {

            // User picture.
            $postuser = new stdClass();
            $postuserfields = explode(',', user_picture::fields());
            $postuser = username_load_fields_from_object($postuser, $post, null, $postuserfields);
            $postuser->id = $post->userid;

            $post->userpicture = $OUTPUT->user_picture($postuser, array('courseid' => $forum->course));

            $modcontext = context_module::instance($cm->id);

            $post->subject = clean_text($post->subject);
            $post->message = clean_text($post->message);

            // User name.
            $post->fullname = fullname($postuser, has_capability('moodle/site:viewfullnames', $modcontext));
            $post->userprofilelink = $CFG->wwwroot . '/user/view.php?id=' . $post->userid . '&amp;course=' . $forum->course;
            $post->courseid = $course->id;
            $post->forumid = $forum->id;

            // Time since post.
            $post->timesince = block_discussion_feed_time_ago($post->modified);

            echo '<div class="indent">';
            if (!isloggedin()) {
                $ownpost = false;
            } else {
                $ownpost = ($USER->id == $post->userid);
            }

            $post->subject = format_string($post->subject);
            $postread = !empty($post->postread);

            $editlink = '';
            $editpostallowed = false;

            // Edit link, if the user is allowed to post.
            $age = time() - $post->created;
            if (isloggedin()) {
                $ownpost = ($post->userid == $USER->id);
            } else {
                $ownpost = false;
            }

            // This is always expected to find the discussion forum.
            if (!$post->parent && $forum->type == 'news' && $post->timestart > time()) {
                $age = 0;
            }

            $modcontext = context_module::instance($cm->id);
            $canedit = has_capability('mod/forum:editanypost', $modcontext);

            if (($ownpost && $age < $CFG->maxeditingtime) || $canedit) {
                $editlink = new moodle_url('/mod/forum/post.php', array('edit' => $post->id));
                $editpostallowed = true;
            }

            $post->editlink = $editlink;
            $post->editlinktext = get_string('editlinktext', 'block_discussion_feed');
            $post->editpostallowed = $editpostallowed;

            $post->canreply = $canreply;

            $discussionpostsjson = new stdClass();
            $discussionpostsjson = $post;

            if ($maxpostlengthtodisplay) {
                $messagelength = strlen($post->message);
                $originalmessage = $post->message;
                if (strlen($post->message) > $maxpostlengthtodisplay) {
                    $excerpt = mb_strimwidth(format_string($post->message), 0, $maxpostlengthtodisplay, ' ...', 'utf-8');
                    $fullmessage = $post->message;

                    $readmoretext = get_string('readmoretext', 'block_discussion_feed');
                    $showlesstext = get_string('showlesstext', 'block_discussion_feed');

                    $showlesslink = '<a href="#" ' .
                                      ' title="' . $showlesstext . '" class="block_discussion_feed_show_less" ' .
                                      ' id="show-less-link_' . $post->id . '" ' . ' data-post-id="' .
                                      $post->id . '">' . $showlesstext . '</a>';

                    $post->message = '<div id="post-text-excerpt_' . $post->id . '">' . $excerpt . ' <a href="#" ' .
                                      ' title="' . $readmoretext . '" class="block_discussion_feed_read_more" id="read-more-link_' .
                                      $post->id . '" ' . ' data-post-id="' . $post->id . '">' . $readmoretext . '</a></div>' .
                                      '<div id="post-text-hidden_' . $post->id .
                                      '" class="block_discussion_feed_hidden">' . $fullmessage . '</div>' . $showlesslink;
                }

            }
            echo $OUTPUT->render_from_template('block_discussion_feed/discussion-post', $discussionpostsjson);

            block_discussion_feed_get_posts_nested($course, $cm, $forum, $discussion, $post, $forumtracked, $posts, $canreply);
            echo "</div>\n";
        }

    }

}

/**
 * Return any groups for the current user for the forum.
 *
 * @param object   $cm              Relevant course module
 * @param object   $course          Course object
 * @param object   $forum           Forum object
 * @param object   $modcontext      Mod context
 *
 * @return string  Return relevant groups (group id => group name) in an array
 */
function block_discussion_feed_group_info($cm, $course, $forum, $modcontext) {

    $content = array();

    if ($groupmode = groups_get_activity_groupmode($cm, $course)) {
        $groupdata = groups_get_activity_allowed_groups($cm);

        $groupinfo = array();
        foreach ($groupdata as $groupid => $group) {
            // Check whether this user can post in this group.
            // We must make this check because all groups are returned for a visible grouped activity.
            if (forum_user_can_post_discussion($forum, $groupid, null, $cm, $modcontext)) {
                // Build the data for the groupinfo select.
                $groupinfo[$groupid] = $group->name;
            } else {
                unset($groupdata[$groupid]);
            }
        }
        $groupcount = count($groupinfo);

        // Check whether a user can post to all of their own groups. This code below for checking is
        // taken from mod/forum/classes/post_form.php.

        // Posts to all of my groups are copied to each group that the user is a member of. Certain conditions must be met.
        // 1) It only makes sense to allow this when a user is in more than one group.
        // Note: This check must come before we consider adding accessallgroups, because that is not a real group.
        $canposttoowngroups = $groupcount > 1;

        // 2) Important: You can *only* post to multiple groups for a top level post. Never any reply.

        // 3) You also need the canposttoowngroups capability.  Unused at present.
        $canposttoowngroups = $canposttoowngroups && has_capability('mod/forum:canposttomygroups', $modcontext);

        // Check whether this user can post to all groups.
        // Posts to the 'All participants' group go to all groups, not to each group in a list.
        // It makes sense to allow this, even if there currently aren't any groups because there may be in the future.
        if (forum_user_can_post_discussion($forum, -1, null, $cm, $modcontext)) {
            // Note: We must reverse in this manner because array_unshift renumbers the array.
            $groupinfo = array_reverse($groupinfo, true );
            $groupinfo[-1] = get_string('allparticipants');
            $groupinfo = array_reverse($groupinfo, true );
            $groupcount++;
        }

        // Determine whether the user can select a group from the dropdown. The dropdown is available for several reasons.
        // 1) This is a new post (not an edit), and there are at least two groups to choose from.
        $canselectgroupfornew = empty($post->edit) && $groupcount > 1;

        // 2) This is editing of an existing post and the user is allowed to movediscussions.
        // We allow this because the post may have been moved from another forum where groups are not available.
        // We show this even if no groups are available as groups *may* have been available but now are not.
        $canselectgroupformove = $groupcount && has_capability('mod/forum:movediscussions', $modcontext);

        // Important: You can *only* change the group for a top level post. Never any reply.
        $canselectgroup = ($canselectgroupfornew || $canselectgroupformove);

        if ($canselectgroup) {
            $content = $groupinfo;
        } else {
            // Check user is in at least one group.
            if ($groupcount > 0) {
                $content = array($groupid => format_string($groupdata[$groupid]->name));
            }
        }
    }

    return $content;
}

/**
 * Get new disccussion form used on a course page.
 *
 * @param object   $cm              Relevant course module
 * @param object   $forum           Forum object
 * @param object   $course          Course object
 * @param int      $buttonid        Button id to be assigned to button in rendering
 * @param int      $parentpostid    Parent post id
 *
 * @return string Return discussion content.
 */
function block_discussion_feed_display_new_discussion_form($cm, $forum, $course, $buttonid, $parentpostid = 0) {
    global $DB, $OUTPUT;

    $content = '';

    $context = context_module::instance($cm->id);

    $replyformdata = new stdClass();

    // Make sure user can post here.  This is taken from mod/forum/post.php.
    $groupdata = block_discussion_feed_group_info($cm, $course, $forum, $context);
    $groupcount = count($groupdata);
    $replyformdata->showmultiplegroups = false;
    $replyformdata->showsinglegroup = false;

    // Check if groups were returned for use in the template.
    if (count($groupdata) > 1) {
        $replyformdata->showmultiplegroups = true;
    } else if (count($groupdata) == 1) {
        $replyformdata->showsinglegroup = true;
    }

    $replyformdata->groupdata = array();
    if ($groupdata > 0) {
        foreach ($groupdata as $key => $value) {
            $replyformdata->groupdata[] = array('id' => $key, 'name' => $value);
        }
    }

    $replyformdata->forumid = $forum->id;
    $replyformdata->courseid = $cm->course;
    $replyformdata->contextid = $context->id;
    $replyformdata->buttonid = $buttonid;
    $replyformdata->parentpostid = $parentpostid;
    $replyformdata->forumname = $forum->name;
    $replyformdata->forumlink = new moodle_url('/mod/forum/view.php?f=' . $forum->id);
    $replyformdata->postingtotext = get_string('postingtotext', 'block_discussion_feed');
    $replyformdata->addasubjectplaceholder = get_string('addasubjecttext', 'block_discussion_feed');
    $replyformdata->addapostplaceholder = get_string('addaposttext', 'block_discussion_feed');

    $content .= $OUTPUT->render_from_template('block_discussion_feed/discussion-form', $replyformdata);

    return $content;
}

/**
 * Get the time elapsed since $time.
 *
 * @param  int    $time  The time to evaluate (in epoch)
 *
 * @return string Formatted string of time elapsed
 *
 */
function block_discussion_feed_time_ago($time) {

    $out    = ''; // What we will print out.
    $now    = time();
    $diff   = $now - $time; // Difference between the current and the provided dates.

    if ($diff < 60) {

        // It happened now.
        return TIMEBEFORE_NOW;

    } else if ($diff < 3600) {

        // It happened X minutes ago.
        return str_replace( '{num}', ( $out = round( $diff / 60 ) ), $out == 1 ? TIMEBEFORE_MINUTE : TIMEBEFORE_MINUTES );

    } else if ($diff < 3600 * 24) {

        // It happened X hours ago.
        return str_replace( '{num}', ( $out = round( $diff / 3600 ) ), $out == 1 ? TIMEBEFORE_HOUR : TIMEBEFORE_HOURS );

    } else if ($diff < 3600 * 24 * 2) {

        // It happened yesterday.
        return TIMEBEFORE_YESTERDAY;

    } else {

        // Falling back on a usual date format as it happened later than yesterday.
        return strftime( date( 'Y', $time ) == date( 'Y' ) ? TIMEBEFORE_FORMAT : TIMEBEFORE_FORMAT_YEAR, $time );

    }
}


/**
 * Get forum object from id.  Uses local caching to save dB calls.
 *
 * @param  int    $forumid  Forum id.
 * @param  int    $strictness  See lib/dml/moodle_database.php. Default is IGNORE_MISSING for flexibility.
 *
 * @return object Forum object
 *
 */
function block_discussion_get_forum_from_id($forumid, $strictness = IGNORE_MISSING) {
    global $DB;
    static $forumarray = array();

    if (isset($forumarray['forumid'])) {
        return $forumarray['forumid'];
    }

    // Remember if $strictness is MUST_EXIST then this next statement will throw exception if the record doesn't exist (obviously).
    $forum = $DB->get_record('forum', array('id' => $forumid), '*', $strictness);

    if ($forum) {
        $forumarray['forumid'] = $forum;
        return $forumarray['forumid'];
    } else {
        return false;
    }

}