<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains language strings used for the Discussion Feed block
 *
 * @package block_discussion_feed
 * @copyright 2018 Manoj Solanki
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Discussion Feed';
$string['blocktitle'] = 'Discussion Feed';

$string['discussion_feed:addinstance'] = 'Add a Discussion Feed block';
$string['discussion_feed:myaddinstance'] = 'Add a Discussion Feed block to the My Moodle page.';

$string['title'] = 'Discussion Feed';
$string['titledesc'] = 'Title for Discussion Feed block.  Leave empty to not show one (default).';

$string['threadlinktext'] = 'View thread';
$string['editlinktext'] = 'Edit post';

$string['addasubjecttext'] = 'Add a subject';
$string['addaposttext'] = 'Add a message here to start a new discussion';

$string['discussionagedays'] = 'Days back to show latest posts for';
$string['discussionagedaysdesc'] = 'Enter the number of days in the past to show latest posts.';

$string['maxpostlengthtodisplay'] = 'Max length of post to show';
$string['maxpostlengthtodisplaydesc'] = 'Max length of post to show to limit the number of characters.';

$string['postedintext'] = 'Posted in ';
$string['coursetext'] = 'Course: ';
$string['lastupdatedtext'] = 'Last updated ';

$string['blockheadertitle'] = 'Discussion';

$string['viewcommentstext'] = 'View comments';
$string['nocommentstext'] = 'No comments yet';
$string['replytext'] = 'Reply';
$string['hidecommentstext'] = 'Hide comments';
$string['hidetext'] = 'Hide';

$string['readmoretext'] = 'Read more';
$string['showlesstext'] = 'Show less';

$string['postingtotext'] = 'Posting to';

$string['showexpandtoggle'] = 'Limit height and show expand toggle';
$string['showexpandtoggledesc'] = 'Limit height and show expand toggle.';

$string['displayinsections'] = 'Show discussion feed in sections';
$string['displayinsectionsdesc'] = 'Show discussion feed in section pages.';

$string['excludeforumids'] = 'Exclude forums';
$string['excludeforumidsdesc'] = 'A list of forum ids to exclude from the feed on the dashboard page. This is to allow exclusion of high traffic forums that contain lots of new posts.
                                  This can take a single ID, or a comma-separated list of ids. E.g. 2,81,42';

$string['privacy:metadata'] = 'The Discussion Feed block does not store or collect any data. It retrieves data in the core Moodle forums for displaying to a user.';

$string['charactersremainingtext'] = ' characters remaining';
$string['errortitletext'] = 'Error message: ';
$string['errordisplayreplyformtext'] = 'Error displaying reply form';
$string['postcouldnotbeaddedtext'] = 'Your post could not be added';
$string['pleaseaddmessagetext'] = 'Please add a message';
$string['thankyouforpostingtext'] = 'Thank you for posting';
$string['pleaseaddsubjecttext'] = 'Please add a subject';
$string['errordisplayingdiscussiontext'] = 'Error displaying discussion';

$string['nocontentdisplaymessagetext'] = 'Any recent discussions from your modules will appear here. If there are no discussions visible, you could always consider starting one within your modules!';
$string['displaynoforumexistsmessagetext'] = 'No valid forum found here to use for discussions';
/*
 * Cache settings.
 */
$string['usecaching'] = 'Use caching';
$string['usecachingdesc'] = 'Switch on caching (uses Moodle cache API). Strongly recommended! Particularly if you have a larger site.';
$string['cachingttl'] = 'Cache Expiry time';
$string['cachingttldesc'] = 'Cache expiry time in seconds (TTL)';