# Moodle Discussion Feed #

This block displays the latest discussions from a course on a frontpage, dashboard page, or course home page.  It will allow a user to reply to any post.  In addition, on a course home page, it will allow a user to create a new discussion if there is a forum of type "general forum" available within the course.  This block uses functionality from the course_discuss block and a forum is linked to a course implicitly by the block so it can be used to store any new discussions. 

# Guidelines for use #

*Note: Each course will require a valid forum activity module and the block plugin course_discuss will need to be installed prior to this plugin.*

As per the course_discuss block, this plugin will silently assign itself to a relevant forum in the course to use when the block is first rendered on a course page, if it can and 
if one is not assigned already. 

The block should be added to a block region that will appear on the frontpage, dashboard page and course homepage

When using Adaptable, this can be achieved by adding the block to the dashboard page in the desired block region.  After that, also add it to block region "Course page slider region" that appears on the frontpage in the admin section.  Configure this block to appear on all pages.  Then, in any course page, configure the block to only appear on any course main page.  This is very important, as the block will otherwise attempt to render twice on the dashboard page, potentially causing performance issues.

Please be careful not to add this to block regions in way that could cause the block to be loaded more than once on a page!

## Other points to note when using this plugin ##

This plugin has been designed to work with the Adaptable theme, as Adaptable contains block regions that appear on the relevant course pages for sections, books and pages. 
It may work with other themes that can support the same features.

Version 1.3.1 (2019030700)

### How do I get set up? ###

Installs at /blocks/discussion_feed

## Settings ##

Site-wide configuration options are available under:  Site Administration > Plugins > Blocks > Course Discuss

The following settings are available:

- Days back to show latest posts for.  This is the number of days in the past to show latest posts.

- Max length of post to show. Maximum length of post to show to limit the number of characters.

- Use caching.  Switch on caching (uses Moodle cache API). This is strongly recommended as this block does make quite a few database calls! 

- Cache Expiry time. Cache expiry time in seconds (TTL).

- Show discussion feed in sections

- Limit height and show expand toggle.  This allows you to limit the height of the block and show an expand button to increase the height.

- Exclude Forums. A list of any forum ids (comma-separated) to exclude from showing on the dashboard page.


### Language strings ###

Please note that from version 1.3.1, the following are now language strings instead of settings (in block_discussion_feed.php):

- Message to display if no content available. Message displayed when no forum updates available for showing on dashboard. (language pack string name: "nocontentdisplaymessagetext")

- Message to display if no Forum exists. Message displayed when no forum exists when the block appears on a course page. (language pack string name: "displaynoforumexistsmessagetext")

### Emoji and Full Unicode support ###

To ensure that emojis can be used in messages, please read the instructions here to ensure your server is set up appropriately: https://docs.moodle.org/36/en/MySQL_full_unicode_support

### Compatibility ###

- Moodle 3.4, 3.5, 3.6

This block has been developed and tested with Adaptable 1.7.2 onwards, which includes a block region that will specifically appear on the relevant pages.  It may work 
with other themes, however it will need to appear in an appropriate block region that renders on a frontpage, dashboard page and course homepage.

### Contribution ###

Developed by:

 * Manoj Solanki (Coventry University)

Co-maintained by:

 * Jeremy Hopkins (Coventry University)
 

 ### Licenses ###

Licensed under: GPL v3 (GNU General Public License) - http://www.gnu.org/licenses
